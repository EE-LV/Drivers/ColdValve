﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="21008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="internalStuff" Type="Folder">
			<Item Name="SDEXDriver.lvlib" Type="Library" URL="../../SDEX/SDEXDriver.lvlib"/>
		</Item>
		<Item Name="ColdValveDriver.lvlib" Type="Library" URL="../ColdValveDriver.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="HVSwitch2Stress" Type="EXE">
				<Property Name="App_applicationGUID" Type="Str">{B19BA9F3-034F-481E-AD11-BC59E2026515}</Property>
				<Property Name="App_applicationName" Type="Str">HVSwitch2Stress.exe</Property>
				<Property Name="App_companyName" Type="Str">GSI Darmstadt</Property>
				<Property Name="App_fileVersion.major" Type="Int">1</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{2EFF9BA7-C11D-47FE-BE0E-3346098B8469}</Property>
				<Property Name="App_INI_GUID" Type="Str">{8775C1D1-2DE3-4DE6-8A6A-E8944604E56A}</Property>
				<Property Name="App_internalName" Type="Str">My Application</Property>
				<Property Name="App_legalCopyright" Type="Str">Copyright © 2007 GSI Darmstadt</Property>
				<Property Name="App_productName" Type="Str">HVSwitch2Stress</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HVSwitch2Stress</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../hvSwitch2/builds/.exe</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../hvSwitch2/builds/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[2].path" Type="Path">../hvSwitch2/builds</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2285545C-E12F-4B1C-ACDC-52A39E165AD6}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/public</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/private</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/examples</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str"></Property>
				<Property Name="TgtF_targetfileName" Type="Str">.exe</Property>
			</Item>
			<Item Name=" HVSwitch2Stress2" Type="EXE">
				<Property Name="App_applicationGUID" Type="Str">{CE89F035-228B-4169-84DE-A5E17DC83FCE}</Property>
				<Property Name="App_applicationName" Type="Str">HVSwitch2Stress2.exe</Property>
				<Property Name="App_companyName" Type="Str">GSI Darmstadt</Property>
				<Property Name="App_fileVersion.major" Type="Int">1</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{1994901C-2CF9-44D6-84AF-54D58B3791E9}</Property>
				<Property Name="App_INI_GUID" Type="Str">{92AEFAE2-9E48-4839-8D49-1543A937E11A}</Property>
				<Property Name="App_internalName" Type="Str">My Application</Property>
				<Property Name="App_legalCopyright" Type="Str">Copyright © 2007 GSI Darmstadt</Property>
				<Property Name="App_productName" Type="Str">HVSwitch2Stress2</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_buildSpecName" Type="Str"> HVSwitch2Stress2</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../hvSwitch2/builds/.exe</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../hvSwitch2/builds/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[2].path" Type="Path">../hvSwitch2/builds</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2285545C-E12F-4B1C-ACDC-52A39E165AD6}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib</Property>
				<Property Name="Source[1].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[1].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/public</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/private</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/examples</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">6</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str"></Property>
				<Property Name="TgtF_targetfileName" Type="Str">.exe</Property>
			</Item>
			<Item Name="HVSwitch2Test" Type="EXE">
				<Property Name="App_applicationGUID" Type="Str">{5C94E4E7-319B-4069-BBD3-49EA863FDD81}</Property>
				<Property Name="App_applicationName" Type="Str">HVSwitch2Test.exe</Property>
				<Property Name="App_companyName" Type="Str">GSI Darmstadt</Property>
				<Property Name="App_fileVersion.build" Type="Int">99</Property>
				<Property Name="App_fileVersion.major" Type="Int">99</Property>
				<Property Name="App_fileVersion.minor" Type="Int">99</Property>
				<Property Name="App_fileVersion.patch" Type="Int">99</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{2D6C72DE-C70E-42FF-9034-38ED88BB187A}</Property>
				<Property Name="App_INI_GUID" Type="Str">{2FB56C30-33C7-400C-915D-E41BE25E433A}</Property>
				<Property Name="App_internalName" Type="Str">My Application</Property>
				<Property Name="App_legalCopyright" Type="Str">Copyright © 2010 GSI Darmstadt</Property>
				<Property Name="App_productName" Type="Str">My Application</Property>
				<Property Name="App_serverType" Type="Int">1</Property>
				<Property Name="Bld_buildSpecName" Type="Str">HVSwitch2Test</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeTypedefs" Type="Bool">true</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Destination[0].destName" Type="Str">.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/HVSwitch2Test/.exe</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/HVSwitch2Test/data</Property>
				<Property Name="Destination[2].destName" Type="Str">Destination Directory</Property>
				<Property Name="Destination[2].path" Type="Path">../builds/NI_AB_PROJECTNAME/HVSwitch2Test</Property>
				<Property Name="DestinationCount" Type="Int">3</Property>
				<Property Name="Source[0].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[0].Container.applyProperties" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{2285545C-E12F-4B1C-ACDC-52A39E165AD6}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/internalStuff</Property>
				<Property Name="Source[1].type" Type="Str">Container</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref"></Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[10].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/internalStuff/SDEXDriver.lvlib</Property>
				<Property Name="Source[2].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[2].type" Type="Str">Library</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/internalStuff/SDEXDriver.lvlib/typedefs</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/internalStuff/SDEXDriver.lvlib/public</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/internalStuff/SDEXDriver.lvlib/private</Property>
				<Property Name="Source[5].type" Type="Str">Container</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib</Property>
				<Property Name="Source[6].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[6].type" Type="Str">Library</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/public</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/private</Property>
				<Property Name="Source[8].type" Type="Str">Container</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/ColdValveDriver.lvlib/examples</Property>
				<Property Name="Source[9].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">11</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str"></Property>
				<Property Name="TgtF_targetfileName" Type="Str">.exe</Property>
			</Item>
			<Item Name="HVSwitch2TestInstaller" Type="Installer">
				<Property Name="arpCompany" Type="Str">GSI Darmstadt</Property>
				<Property Name="arpURL" Type="Str">http://www.gsi.de</Property>
				<Property Name="AutoIncrement" Type="Bool">true</Property>
				<Property Name="BldInfo.Count" Type="Int">1</Property>
				<Property Name="BldInfo[0].Dir" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="BldInfo[0].Tag" Type="Ref">/My Computer/Build Specifications/HVSwitch2Test</Property>
				<Property Name="BuildLabel" Type="Str">HVSwitch2TestInstaller</Property>
				<Property Name="BuildLocation" Type="Path">../../Builds/HVSwitch2Driver/HVSwitch2TestInstaller</Property>
				<Property Name="DirInfo.Count" Type="Int">1</Property>
				<Property Name="DirInfo.DefaultDir" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="DirInfo[0].DirName" Type="Str">HVSwitch2Driver</Property>
				<Property Name="DirInfo[0].DirTag" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="DirInfo[0].ParentTag" Type="Str">{3912416A-D2E5-411B-AFEE-B63654D690C0}</Property>
				<Property Name="DistID" Type="Str">{2A64A6A3-9D2A-4D48-9F76-5D3EFBC10C11}</Property>
				<Property Name="DistParts.Count" Type="Int">4</Property>
				<Property Name="DistPartsInfo[0].FlavorID" Type="Str">DefaultFull</Property>
				<Property Name="DistPartsInfo[0].ProductID" Type="Str">{45FA54F6-8574-49D2-9E2D-0BDDE6237822}</Property>
				<Property Name="DistPartsInfo[0].ProductName" Type="Str">NI LabVIEW Run-Time Engine 8.2.1</Property>
				<Property Name="DistPartsInfo[0].UpgradeCode" Type="Str">{B5171839-26E3-48D9-9FD6-AF7F39055146}</Property>
				<Property Name="DistPartsInfo[1].FlavorID" Type="Str">_full_</Property>
				<Property Name="DistPartsInfo[1].ProductID" Type="Str">{2B5D112B-5784-43CF-A2EB-1B89156385E7}</Property>
				<Property Name="DistPartsInfo[1].ProductName" Type="Str">NI Measurement &amp; Automation Explorer 4.6.1</Property>
				<Property Name="DistPartsInfo[1].UpgradeCode" Type="Str">{AE940F24-CC0E-4148-9A96-10FB04D9796D}</Property>
				<Property Name="DistPartsInfo[2].FlavorID" Type="Str">_full_</Property>
				<Property Name="DistPartsInfo[2].ProductID" Type="Str">{35444DD5-DFA8-44AF-B299-C489071D0B0D}</Property>
				<Property Name="DistPartsInfo[2].ProductName" Type="Str">NI-Serial 3.4</Property>
				<Property Name="DistPartsInfo[2].UpgradeCode" Type="Str">{01D82F43-B48D-46FF-8601-FC4FAAE20F41}</Property>
				<Property Name="DistPartsInfo[3].FlavorID" Type="Str">_deployment_</Property>
				<Property Name="DistPartsInfo[3].ProductID" Type="Str">{D6FC9FA9-3386-409A-8D62-EE026CA721D1}</Property>
				<Property Name="DistPartsInfo[3].ProductName" Type="Str">NI-VISA Runtime 4.5</Property>
				<Property Name="DistPartsInfo[3].UpgradeCode" Type="Str">{8627993A-3F66-483C-A562-0D3BA3F267B1}</Property>
				<Property Name="FileInfo.Count" Type="Int">3</Property>
				<Property Name="FileInfo[0].DirTag" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="FileInfo[0].FileName" Type="Str">HVSwitch2Test.exe</Property>
				<Property Name="FileInfo[0].FileTag" Type="Str">{5C94E4E7-319B-4069-BBD3-49EA863FDD81}</Property>
				<Property Name="FileInfo[0].Type" Type="Int">3</Property>
				<Property Name="FileInfo[0].TypeID" Type="Ref">/My Computer/Build Specifications/HVSwitch2Test</Property>
				<Property Name="FileInfo[1].DirTag" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="FileInfo[1].FileName" Type="Str">HVSwitch2Test.aliases</Property>
				<Property Name="FileInfo[1].FileTag" Type="Str">{2D6C72DE-C70E-42FF-9034-38ED88BB187A}</Property>
				<Property Name="FileInfo[1].Type" Type="Int">3</Property>
				<Property Name="FileInfo[1].TypeID" Type="Ref">/My Computer/Build Specifications/HVSwitch2Test</Property>
				<Property Name="FileInfo[2].DirTag" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="FileInfo[2].FileName" Type="Str">HVSwitch2Test.ini</Property>
				<Property Name="FileInfo[2].FileTag" Type="Str">{2FB56C30-33C7-400C-915D-E41BE25E433A}</Property>
				<Property Name="FileInfo[2].Type" Type="Int">3</Property>
				<Property Name="FileInfo[2].TypeID" Type="Ref">/My Computer/Build Specifications/HVSwitch2Test</Property>
				<Property Name="InstSpecBitness" Type="Str">32-bit</Property>
				<Property Name="InstSpecVersion" Type="Str">21008000</Property>
				<Property Name="LicenseFile" Type="Ref"></Property>
				<Property Name="MSI_osCheck" Type="Int">0</Property>
				<Property Name="OSCheck" Type="Int">1</Property>
				<Property Name="OSCheck_Vista" Type="Bool">false</Property>
				<Property Name="ProductName" Type="Str">HVSwitch2TestInstaller</Property>
				<Property Name="ProductVersion" Type="Str">1.0.1</Property>
				<Property Name="ReadmeFile" Type="Ref"></Property>
				<Property Name="ShortcutInfo.Count" Type="Int">1</Property>
				<Property Name="ShortcutInfo[0].DirTag" Type="Str">{B9E310F1-839C-48B7-8CAE-33000780C26E}</Property>
				<Property Name="ShortcutInfo[0].FileTag" Type="Str">{5C94E4E7-319B-4069-BBD3-49EA863FDD81}</Property>
				<Property Name="ShortcutInfo[0].FileTagDir" Type="Str">{EBB89026-B6EF-4E10-BF45-D6B9B5F8C2D6}</Property>
				<Property Name="ShortcutInfo[0].Name" Type="Str">HVSwitch2Test</Property>
				<Property Name="ShortcutInfo[0].SubDir" Type="Str">HVSwitch2TestInstaller</Property>
				<Property Name="UpgradeCode" Type="Str">{AE7906CE-EF92-4819-9670-FAEC88190D3A}</Property>
			</Item>
		</Item>
	</Item>
</Project>
