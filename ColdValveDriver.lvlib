﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="14008000">
	<Property Name="Alarm Database Computer" Type="Str">localhost</Property>
	<Property Name="Alarm Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Alarm Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Data Lifespan" Type="UInt">3650</Property>
	<Property Name="Database Computer" Type="Str">localhost</Property>
	<Property Name="Database Name" Type="Str">E__Program_Files_National_Instruments_LabVIEW_8_2_data</Property>
	<Property Name="Database Path" Type="Str">E:\Program Files\National Instruments\LabVIEW 8.2\data</Property>
	<Property Name="Enable Alarms Logging" Type="Bool">false</Property>
	<Property Name="Enable Data Logging" Type="Bool">false</Property>
	<Property Name="NI.Lib.Description" Type="Str">This is the instrument driver for 2nd generation of the GSI HV switch. A HV switch can be used to switch between two programmable voltages depending on an external hardware signal. The 2nd generation uses the SDEX interface (presently via RS485 serial bus) and has been developed by Harald Hahn, ECOS/GSI. The VI Tree displays all the user-callable VIs of the instrument driver in an organized table. All user-callable VIs are based on the VIs of the SDEX communication driver.

Please note, that the module addresse must be given in HEX format. The module addresses in the first crate are 08, 10, 20, 40, 28 and 00. The offset to the module addresses can be changed on the backplane of a crate (for "old" create, the coding plug needs to be changed). Typically, the modules use a Baud rate of 115200 Baud.

author: Dietrich Beck, GSI
maintainer: Dennis Neidherr, GSI

Copyright (C) 
Gesellschaft f. Schwerionenforschung, GSI
Planckstr. 1
64291 Darmstadt
Germany

Contact: http://www-wnt.gsi.de/labview, d.beck@gsi.de

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details (http://www.gnu.org).
</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!-0!!!*Q(C=V:3`&lt;B."%-9`/R1A1:%O"15S4&gt;!VB'FJ%'\IS&gt;65UV.&amp;&gt;!9E&amp;#3%!A+*?97]!5I47D3PE&amp;@)%S!&gt;0_`NH?U)C*"!ALW-=`[_W&gt;H@`P&amp;+J6W6LKC\L%G&gt;@NC7FH\O^U9X2*`6&gt;?K'^Z5`;HWM=N@[LH7J^ACR]LOB1(E@RPZ6W`#&lt;&gt;JBUWVTU?[^N3GP(H/*P?-UKJ?X^^M&gt;GWX:.TV_&amp;480JV`H&gt;LH*&lt;`V6X^*PG&gt;L04&amp;GP.&lt;=?V&lt;TYXT=Y/'=W[OTY_&gt;5H9'@W_?_6HX+\070HNWPT,?SGSKD_MY,A_@5;X[9\L/W2UG_[&amp;^3&gt;DU^X9PX\_'_\&amp;`7]OO,^Z0PZ,`^+@NR;8*PXF*EU++:&amp;%%%Z9@`I30&gt;%40&gt;%40&gt;%$0&gt;!$0&gt;!$0&gt;!&gt;X&gt;%&gt;X&gt;%&gt;X&gt;%.X&gt;!.X&gt;!.X9&lt;\"FXI1J?6#S1:0"EI+:I53**"5@+2]#1]#5`#QV=F0!F0QJ0Q*$SE+/&amp;*?"+?B#@BI:M3HI1HY5FY%BZ+&amp;:)M$TI]#1`F&amp;@!%0!&amp;0Q"0Q-+1#HA!A'#QI("1"1Y%:P!1]!5`!Q[M#HI!HY!FY!BZM"4Q"4]!4]!1]&gt;#GL%I6G_;$$1REZ0![0Q_0Q/$S5FM0D]$A]$I`$QX"S?"Q?"]):U#E/AJR/4I,TR?&amp;R?0AHB]@B=8A=(I=(K_S1FZ6:UCQ@&gt;(A-(I0(Y$&amp;Y$"Z+S/!R?!Q?A]@AI;Q-(I0(Y$&amp;Y$"['EM&amp;D]"A]"IAR+-0,+':U.*)-Q?$BLZQ7+\M5B=4+5X_;`5&amp;609#K"UPVQ+A?".5.6NUYV1V287D6"62&gt;'.5*KUZ%&amp;6"V9.7#KIE[Z`/-/#'/C50CA.AHZM3-W&amp;ZW`=/*Z_@H/DM\U]H*C9[0DX6Y?+C$AQ0N\_^L0J^L.JNJ?XN\P+VO]@2N-NR,2^)?]3A?0L\W=@ZY\]0TX1=@8OU_7Z#TW0IW7&gt;T@_P,S6%`@;OPVO^/&lt;VT\&gt;@8,H`9V&amp;(%WHDY[GE\XX5XVZ]W*L]H*W@&lt;,Y?H?SO(=[7&gt;SALXY&gt;`&lt;XU,^S.GKJ&lt;O_::I_`9F=H*!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">335577088</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.16.0.0</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Property Name="OdbcAlarmLoggingTableName" Type="Str">NI_ALARM_EVENTS</Property>
	<Property Name="OdbcBooleanLoggingTableName" Type="Str">NI_VARIABLE_BOOLEAN</Property>
	<Property Name="OdbcConnectionRadio" Type="UInt">0</Property>
	<Property Name="OdbcConnectionString" Type="Str"></Property>
	<Property Name="OdbcCustomStringText" Type="Str"></Property>
	<Property Name="OdbcDoubleLoggingTableName" Type="Str">NI_VARIABLE_NUMERIC</Property>
	<Property Name="OdbcDSNText" Type="Str"></Property>
	<Property Name="OdbcEnableAlarmLogging" Type="Bool">false</Property>
	<Property Name="OdbcEnableDataLogging" Type="Bool">false</Property>
	<Property Name="OdbcPassword" Type="Str"></Property>
	<Property Name="OdbcReconnectPeriod" Type="UInt">0</Property>
	<Property Name="OdbcReconnectTimeUnit" Type="Int">0</Property>
	<Property Name="OdbcStringLoggingTableName" Type="Str">NI_VARIABLE_STRING</Property>
	<Property Name="OdbcUsername" Type="Str"></Property>
	<Property Name="SaveStatePeriod" Type="UInt">0</Property>
	<Property Name="Serialized ACL" Type="Bin">&amp;!#!!!!!!!)!"1!&amp;!!!A1%!!!@````]!!".V&lt;H.J:WZF:#"C?82F)'&amp;S=G&amp;Z!!%!!1!!!!A)!!!!#!!!!!!!!!!</Property>
	<Property Name="Use Data Logging Database" Type="Bool">true</Property>
	<Item Name="public" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="ColdValve.Close.vi" Type="VI" URL="../ColdValve.Close.vi"/>
		<Item Name="ColdValve.Error Message.vi" Type="VI" URL="../ColdValve.Error Message.vi"/>
		<Item Name="ColdValve.get library version.vi" Type="VI" URL="../ColdValve.get library version.vi"/>
		<Item Name="ColdValve.Initialize.vi" Type="VI" URL="../ColdValve.Initialize.vi"/>
		<Item Name="ColdValve.ReadDelays.vi" Type="VI" URL="../ColdValve.ReadDelays.vi"/>
		<Item Name="ColdValve.reset.vi" Type="VI" URL="../ColdValve.reset.vi"/>
		<Item Name="ColdValve.Revision Query.vi" Type="VI" URL="../ColdValve.Revision Query.vi"/>
		<Item Name="ColdValve.SetDelays.vi" Type="VI" URL="../ColdValve.SetDelays.vi"/>
		<Item Name="ColdValve.StartTrigger.vi" Type="VI" URL="../ColdValve.StartTrigger.vi"/>
	</Item>
	<Item Name="private" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
		<Item Name="ColdValve.call chain 2 string.vi" Type="VI" URL="../ColdValve.call chain 2 string.vi"/>
	</Item>
	<Item Name="examples" Type="Folder">
		<Property Name="NI.LibItem.Scope" Type="Int">1</Property>
		<Item Name="ColdValve.application example.vi" Type="VI" URL="../ColdValve.application example.vi"/>
	</Item>
	<Item Name="ColdValve.VI Tree.vi" Type="VI" URL="../ColdValve.VI Tree.vi"/>
</Library>
